﻿$(document).ready(function () {
    safeEmail();
    scrollPage();
    $(".parallax").parallax({
        imageSrc: '/images/background1200-opt.jpg',
        speed: 0.4
    });
    $(document).foundation();

});

$(document).scroll(function () {
    fixBar();
});
function safeEmail() {
    $("span.elink").each(
            function () {
                var mailtoText = $(this).attr("data-context");
                mailtoText = mailtoText.replace(/\[at\]/g, "@");
                mailtoText = mailtoText.replace(/\[dot\]/g, ".");
                $(this).html(
                        "<a href=\"mailto:" + mailtoText + "\">" + mailtoText
                        + "</a>");
            });
}
var header;
var fixBar = function () {

    header = $("header").height();

    if ($(window).scrollTop() > header - 5)
    {

        $(".page-header").fadeIn(1500);
    } else
    {

        $(".page-header").fadeOut(500);

    }

};
var scrollPage = function () {
    $(".jumper").on(
            "click",
            function (e) {

                $("body, html").stop();
                e.preventDefault();

                var loc = $(this).data('jumpto');
                var pos = $(loc).offset().top;
                if (pos < 0) {
                    pos = 0;
                }
                scrollTo(pos, 1000);

            });
};
var scrollTo = function (position, speed) {
    $("body, html").animate({
        scrollTop: position
    }, speed);
};
