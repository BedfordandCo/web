
var BedCoBase = Vue.extend(
        {
            created: function () {
                this.fetchData();

            },
            methods:
                    {
                        fetchData: function () {
                           this.getData();

                        },
                        complete: function () {

                        }
                    }
        });

Vue.filter('ftseRename', function (value) {
    if (value === "INDEXFTSE")
    {
        value = "FTSE100";
    }
    return value;
});
Vue.filter('cur-imglink', function (value = "x") {
    try{
        value = value.toLowerCase()
        return "/images/currencies/" + value + ".png"
    } catch {
        console.log("error getting flag")
    }
});
var lastupdate;
var ftse = function () {
    new BedCoBase({
        el: '#ftse-group',
        data: {
            items: []
        },
        created: function () {
            /*setInterval(this.getData, 30000);*/
        },
        methods: {
            getData: function () {
                var self = this;
                $.ajax({
                    url: '#'
                }).done(function (data)
                {
                    if (data.DateString !== lastupdate)
                    {
                        $(".ftse-group .updater").addClass("updating", {duration: 500});
                        self.stocks = data.Stocks;
                        self.date = data.LastUpdated;
                        self.updated = data.DateString;
                        lastupdate = data.DateString;
                        $(".ftse-group .updater").removeClass('updating', {duration: 1000});
                        self.complete();
                    }

                });
            }

        }
    });

};
var xe = function () {
    new BedCoBase({
        el: '#xe-group',
        data: {
            xeOK: false,
            data: []
        },
        created: function () {
            this.getData;
        },
        methods: {
            getData: function () {
                var self = this;
                var url = "https://api.wilde.cloud/api/v1/feeds/xe";
                fetch(url).then(function(response) {
                    response.text().then(function(text) {
                        let xe = JSON.parse(text);
                        self.data = xe.content;
                        self.xeOK = true;
                    });
                });
            }
        }
    });

};
var bbcnews = function () {
    new BedCoBase({
        el: '#bbcnews',
        data: {
            bbcOK: false,
            data: []
        },
        created: function () {
            this.getData;
        },
        methods: {
            getData: function () {
                var self = this;
                var url = "https://api.wilde.cloud/api/v1/feeds/bbcnews";
                fetch(url).then(function(response) {
                response.text().then(function(text) {
                    let rss = JSON.parse(text);
                    self.data = rss.content;
                    self.bbcOK = rss.ok;
                    if (rss.ok) {
                        setTimeout(function(){
                            self.bbcCycle();
                        },1000);
                    }
                });
              });

            },
            bbcCycle: function() {
               $('#bbcfeed').cycle({
                   fx: 'scrollVert',
                   easing: "easeOutSine",
                   sync: true,
                   speed: 2000,
                   slides: '> div',
                   pauseOnHover: true
               });
               $('.bbc-news-feed').css("display", "block");
            }

        }
    });

};

bbcnews();
xe();

// ftse();
